# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors:
#   Quan Zhou <quan@bitergia.com>
#


import argparse
import datetime
import subprocess
import sys


def parse_args():
    """ Parse command line arguments """

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--password",
                        default=None,
                        help="MariaDB password")

    # Required arguments
    required = parser.add_argument_group('required arguments')
    required.add_argument("-d", "--database",
                          required=True,
                          help="MariaDB database name")
    required.add_argument("-u", "--user",
                          required=True,
                          help="MariaDB username")
    required.add_argument("--host",
                          required=True,
                          help="MariaDB host")
    args = parser.parse_args()
    return args


def run_cmd(cmd):
    process = subprocess.Popen(cmd, shell=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    (out, err) = process.communicate()
    errcode = process.returncode
    if errcode == 0:
        print("\t[OK]: " + cmd)
        return out.decode('utf-8')
    else:
        print("\t[ERROR]: " + cmd)
        raise Exception(err.decode('utf-8'))


def do_mysqldump(database, host, user, password=None):
    """Do mysqldump

    :param database: MAriaDB database
    :param host: MariaDB host
    :param user: MariaDB username
    :param password: MariaDB password
    """
    cmd = "mysqldump -u " + user
    if password:
        cmd += " -p" + password
    cmd += " --host " + host + " " + database + " > "+database+".sql"
    run_cmd(cmd)


def do_mysqldump_sh(database, host, user, password=None):
    """Do mysqldump with the options --no-create-info --ignore-tables=<database>.countries

    :param database: MAriaDB database
    :param host: MariaDB host
    :param user: MariaDB username
    :param password: MariaDB password
    """
    cmd = "mysqldump -u " + user
    if password:
        cmd += " -p" + password
    cmd += " --host " + host + " " + database
    cmd += " --no-create-info --skip-set-charset --default-character-set=utf8mb4 --ignore-table=" + database+".countries --complete-insert --insert-ignore > "+database+"_no_table.sql"
    run_cmd(cmd)


def delete_database(database, host, user, password=None):
    """Delete MariaDB database

    :param database: MAriaDB database
    :param host: MariaDB host
    :param user: MariaDB username
    :param password: MariaDB password
    """
    cmd = "mysql -u" + user
    if password:
        cmd += " -p" + password
    cmd += " --host " + host + " -e 'drop database " + database + "'"
    run_cmd(cmd)


def create_database_sh(database, host, user, password):
    """Create database for SortingHat with new charset and collation using the MySQL
    client. Due to the encoding errors found at https://phabricator.bitergia.net/T8411
    it is needed to use a different charset for creating the database.

    :param database: MAriaDB database
    :param host: MariaDB host
    :param user: MariaDB username
    :param password: MariaDB password
    """
    cmd = "mysql -u" + user
    if password:
        cmd += " -p" + password
    cmd += " --host " + host + " -e 'create database " + database + " CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;'"
    run_cmd(cmd)

    cmd2 = "mysql -u" + user
    if password:
        cmd2 += " -p" + password
    cmd2 += " --host " + host + " " + database + " < data/sh_schema.sql"
    run_cmd(cmd2)

def load_database(database, database_sql, host, user, password=None):
    """Load database into MariaDB

    :param database: MariaDB database
    :param database_sql: Name of the dump database
    :param host: MariaDB host
    :param user: MariaDB username
    :param password: MariaDB password
    """

    cmd = "mysql -u" + user
    if password:
        cmd += " -p" + password
    cmd += " --host " + host + " " + database + " < " + database_sql + ".sql"
    run_cmd(cmd)


def check_identities(database, host, user, password=None):
    """Check the number of the identities in the MariaDB database

    :param database: MAriaDB database
    :param host: MariaDB host
    :param user: MariaDB username
    :param password: MariaDB password

    :return: Identities count
    """
    cmd = "mysql -u" + user
    if password:
        cmd += " -p" + password
    cmd += " --host " + host + " " + database + " -e 'select count(*) from identities;'"
    count = run_cmd(cmd).split()[-1]
    return int(count)


def check_dump(origin_count, database, dump_database,  host, user, password):
    """Check if the dump has the same identities count

    :param origin_count: origin identitie count
    :param database: database
    :param dump_database: dump of the database
    :param host: MariaDB host
    :param user: MariaDB user
    :param password: MariaDB password

    :return: True | False
    """
    def __create_database(database, host, user, password):
        cmd = "mysql -u" + user
        if password:
            cmd += " -p" + password
        cmd += " --host " + host + " -e 'create database " + database + ";'"
        run_cmd(cmd)

    __create_database(dump_database, host, user, password)
    load_database(dump_database, database, host, user, password)
    dump_count = check_identities(dump_database, host, user, password)
    print("\tOrigin database identities count: " + str(origin_count))
    print("\tDump database identities count: " + str(dump_count))

    if origin_count == dump_count:
        return True
    return False


def main():
    date = datetime.datetime.today().strftime('%y%m%d')

    args = parse_args()

    user = args.user
    password = args.password
    database = args.database
    host = args.host

    print(database)
    origin_count = check_identities(database, host, user, password)
    do_mysqldump_sh(database, host, user, password)
    do_mysqldump(database, host, user, password)

    # check if the dump is correct
    dump_database = database+"_"+date
    dump_success = check_dump(origin_count, database, dump_database, host, user, password)
    if not dump_success:
        raise Exception("Database dump FAIL")

    print("\tDatabase dump SUCCESS")

    delete_database(database, host, user, password)
    delete_database(dump_database, host, user, password)

    # Create a new database with SortingHat
    create_database_sh(database, host, user, password)
    load_database(database, database+"_no_table", host, user, password)

    final_count = check_identities(database, host, user, password)

    msg = "\tOrigin database identities count: " + str(origin_count) + "\n"
    msg += "\tFinal database identities count: " + str(final_count) + "\n"

    exit_code = 0
    if origin_count != final_count:
        msg += "FAIL\n"
        exit_code = -1
    else:
        msg += "SUCCESS"
    print(msg)
    sys.exit(exit_code)


if __name__ == '__main__':
    main()
