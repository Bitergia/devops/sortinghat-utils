-- MySQL dump 10.15  Distrib 10.0.35-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: encodingsucks_sh
-- ------------------------------------------------------
-- Server version	10.0.23-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `code` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `alpha3` varchar(3) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `_alpha_unique` (`alpha3`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES ('AD','Andorra','AND'),('AE','United Arab Emirates','ARE'),('AF','Afghanistan','AFG'),('AG','Antigua and Barbuda','ATG'),('AI','Anguilla','AIA'),('AL','Albania','ALB'),('AM','Armenia','ARM'),('AO','Angola','AGO'),('AQ','Antarctica','ATA'),('AR','Argentina','ARG'),('AS','American Samoa','ASM'),('AT','Austria','AUT'),('AU','Australia','AUS'),('AW','Aruba','ABW'),('AX','Åland Islands','ALA'),('AZ','Azerbaijan','AZE'),('BA','Bosnia and Herzegovina','BIH'),('BB','Barbados','BRB'),('BD','Bangladesh','BGD'),('BE','Belgium','BEL'),('BF','Burkina Faso','BFA'),('BG','Bulgaria','BGR'),('BH','Bahrain','BHR'),('BI','Burundi','BDI'),('BJ','Benin','BEN'),('BL','Saint Barthélemy','BLM'),('BM','Bermuda','BMU'),('BN','Brunei Darussalam','BRN'),('BO','Bolivia (Plurinational State of)','BOL'),('BQ','Bonaire, Sint Eustatius and Saba','BES'),('BR','Brazil','BRA'),('BS','Bahamas','BHS'),('BT','Bhutan','BTN'),('BV','Bouvet Island','BVT'),('BW','Botswana','BWA'),('BY','Belarus','BLR'),('BZ','Belize','BLZ'),('CA','Canada','CAN'),('CC','Cocos (Keeling) Islands','CCK'),('CD','Congo (Democratic Republic of the)','COD'),('CF','Central African Republic','CAF'),('CG','Congo','COG'),('CH','Switzerland','CHE'),('CI','Côte d\'Ivoire','CIV'),('CK','Cook Islands','COK'),('CL','Chile','CHL'),('CM','Cameroon','CMR'),('CN','China','CHN'),('CO','Colombia','COL'),('CR','Costa Rica','CRI'),('CU','Cuba','CUB'),('CV','Cabo Verde','CPV'),('CW','Curaçao','CUW'),('CX','Christmas Island','CXR'),('CY','Cyprus','CYP'),('CZ','Czech Republic','CZE'),('DE','Germany','DEU'),('DJ','Djibouti','DJI'),('DK','Denmark','DNK'),('DM','Dominica','DMA'),('DO','Dominican Republic','DOM'),('DZ','Algeria','DZA'),('EC','Ecuador','ECU'),('EE','Estonia','EST'),('EG','Egypt','EGY'),('EH','Western Sahara','ESH'),('ER','Eritrea','ERI'),('ES','Spain','ESP'),('ET','Ethiopia','ETH'),('FI','Finland','FIN'),('FJ','Fiji','FJI'),('FK','Falkland Islands (Malvinas)','FLK'),('FM','Micronesia (Federated States of)','FSM'),('FO','Faroe Islands','FRO'),('FR','France','FRA'),('GA','Gabon','GAB'),('GB','United Kingdom of Great Britain and Northern Ireland','GBR'),('GD','Grenada','GRD'),('GE','Georgia','GEO'),('GF','French Guiana','GUF'),('GG','Guernsey','GGY'),('GH','Ghana','GHA'),('GI','Gibraltar','GIB'),('GL','Greenland','GRL'),('GM','Gambia','GMB'),('GN','Guinea','GIN'),('GP','Guadeloupe','GLP'),('GQ','Equatorial Guinea','GNQ'),('GR','Greece','GRC'),('GS','South Georgia and the South Sandwich Islands','SGS'),('GT','Guatemala','GTM'),('GU','Guam','GUM'),('GW','Guinea-Bissau','GNB'),('GY','Guyana','GUY'),('HK','Hong Kong','HKG'),('HM','Heard Island and McDonald Islands','HMD'),('HN','Honduras','HND'),('HR','Croatia','HRV'),('HT','Haiti','HTI'),('HU','Hungary','HUN'),('ID','Indonesia','IDN'),('IE','Ireland','IRL'),('IL','Israel','ISR'),('IM','Isle of Man','IMN'),('IN','India','IND'),('IO','British Indian Ocean Territory','IOT'),('IQ','Iraq','IRQ'),('IR','Iran (Islamic Republic of)','IRN'),('IS','Iceland','ISL'),('IT','Italy','ITA'),('JE','Jersey','JEY'),('JM','Jamaica','JAM'),('JO','Jordan','JOR'),('JP','Japan','JPN'),('KE','Kenya','KEN'),('KG','Kyrgyzstan','KGZ'),('KH','Cambodia','KHM'),('KI','Kiribati','KIR'),('KM','Comoros','COM'),('KN','Saint Kitts and Nevis','KNA'),('KP','Korea (Democratic People\'s Republic of)','PRK'),('KR','Korea (Republic of)','KOR'),('KW','Kuwait','KWT'),('KY','Cayman Islands','CYM'),('KZ','Kazakhstan','KAZ'),('LA','Lao People\'s Democratic Republic','LAO'),('LB','Lebanon','LBN'),('LC','Saint Lucia','LCA'),('LI','Liechtenstein','LIE'),('LK','Sri Lanka','LKA'),('LR','Liberia','LBR'),('LS','Lesotho','LSO'),('LT','Lithuania','LTU'),('LU','Luxembourg','LUX'),('LV','Latvia','LVA'),('LY','Libya','LBY'),('MA','Morocco','MAR'),('MC','Monaco','MCO'),('MD','Moldova (Republic of)','MDA'),('ME','Montenegro','MNE'),('MF','Saint Martin (French part)','MAF'),('MG','Madagascar','MDG'),('MH','Marshall Islands','MHL'),('MK','Macedonia (the former Yugoslav Republic of)','MKD'),('ML','Mali','MLI'),('MM','Myanmar','MMR'),('MN','Mongolia','MNG'),('MO','Macao','MAC'),('MP','Northern Mariana Islands','MNP'),('MQ','Martinique','MTQ'),('MR','Mauritania','MRT'),('MS','Montserrat','MSR'),('MT','Malta','MLT'),('MU','Mauritius','MUS'),('MV','Maldives','MDV'),('MW','Malawi','MWI'),('MX','Mexico','MEX'),('MY','Malaysia','MYS'),('MZ','Mozambique','MOZ'),('NA','Namibia','NAM'),('NC','New Caledonia','NCL'),('NE','Niger','NER'),('NF','Norfolk Island','NFK'),('NG','Nigeria','NGA'),('NI','Nicaragua','NIC'),('NL','Netherlands','NLD'),('NO','Norway','NOR'),('NP','Nepal','NPL'),('NR','Nauru','NRU'),('NU','Niue','NIU'),('NZ','New Zealand','NZL'),('OM','Oman','OMN'),('PA','Panama','PAN'),('PE','Peru','PER'),('PF','French Polynesia','PYF'),('PG','Papua New Guinea','PNG'),('PH','Philippines','PHL'),('PK','Pakistan','PAK'),('PL','Poland','POL'),('PM','Saint Pierre and Miquelon','SPM'),('PN','Pitcairn','PCN'),('PR','Puerto Rico','PRI'),('PS','Palestina, State of','PSE'),('PT','Portugal','PRT'),('PW','Palau','PLW'),('PY','Paraguay','PRY'),('QA','Qatar','QAT'),('RE','Réunion','REU'),('RO','Romania','ROU'),('RS','Serbia','SRB'),('RU','Russian Federation','RUS'),('RW','Rwanda','RWA'),('SA','Saudi Arabia','SAU'),('SB','Solomon Islands','SLB'),('SC','Seychelles','SYC'),('SD','Sudan','SDN'),('SE','Sweden','SWE'),('SG','Singapore','SGP'),('SH','Saint Helena, Ascension and Tristan da Cunha','SHN'),('SI','Slovenia','SVN'),('SJ','Svalbard and Jan Mayen','SJM'),('SK','Slovakia','SVK'),('SL','Sierra Leone','SLE'),('SM','San Marino','SMR'),('SN','Senegal','SEN'),('SO','Somalia','SOM'),('SR','Suriname','SUR'),('SS','South Sudan','SSD'),('ST','Sao Tome and Principe','STP'),('SV','El Salvador','SLV'),('SX','Sint Maarten (Dutch part)','SXM'),('SY','Syrian Arab Republic','SYR'),('SZ','Swaziland','SWZ'),('TC','Turks and Caicos Islands','TCA'),('TD','Chad','TCD'),('TF','French Southern Territories','ATF'),('TG','Togo','TGO'),('TH','Thailand','THA'),('TJ','Tajikistan','TJK'),('TK','Tokelau','TKL'),('TL','Timor-Leste','TLS'),('TM','Turkmenistan','TKM'),('TN','Tunisia','TUN'),('TO','Tonga','TON'),('TR','Turkey','TUR'),('TT','Trinidad and Tobago','TTO'),('TV','Tuvalu','TUV'),('TW','Taiwan, Province of China','TWN'),('TZ','Tanzania, United Republic of','TZA'),('UA','Ukraine','UKR'),('UG','Uganda','UGA'),('UM','United States Minor Outlying Islands','UMI'),('US','United States of America','USA'),('UY','Uruguay','URY'),('UZ','Uzbekistan','UZB'),('VA','Holy See','VAT'),('VC','Saint Vincent and the Grenadines','VCT'),('VE','Venezuela (Bolivarian Republic of)','VEN'),('VG','Virgin Islands (British)','VGB'),('VI','Virgin Islands (U.S.)','VIR'),('VN','Viet Nam','VNM'),('VU','Vanuatu','VUT'),('WF','Wallis and Futuna','WLF'),('WS','Samoa','WSM'),('YE','Yemen','YEM'),('YT','Mayotte','MYT'),('ZA','South Africa','ZAF'),('ZM','Zambia','ZMB'),('ZW','Zimbabwe','ZWE');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domains_organizations`
--

DROP TABLE IF EXISTS `domains_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domains_organizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_top_domain` tinyint(1) DEFAULT NULL,
  `organization_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_domain_unique` (`domain`),
  KEY `organization_id` (`organization_id`),
  CONSTRAINT `domains_organizations_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domains_organizations`
--

LOCK TABLES `domains_organizations` WRITE;
/*!40000 ALTER TABLE `domains_organizations` DISABLE KEYS */;
/*!40000 ALTER TABLE `domains_organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enrollments`
--

DROP TABLE IF EXISTS `enrollments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enrollments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `uuid` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `organization_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_period_unique` (`uuid`,`organization_id`,`start`,`end`),
  KEY `organization_id` (`organization_id`),
  CONSTRAINT `enrollments_ibfk_1` FOREIGN KEY (`uuid`) REFERENCES `uidentities` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `enrollments_ibfk_2` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrollments`
--

LOCK TABLES `enrollments` WRITE;
/*!40000 ALTER TABLE `enrollments` DISABLE KEYS */;
/*!40000 ALTER TABLE `enrollments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `identities`
--

DROP TABLE IF EXISTS `identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `identities` (
  `id` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `username` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `source` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `uuid` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `last_modified` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_identity_unique` (`name`,`email`,`username`,`source`),
  KEY `uuid` (`uuid`),
  CONSTRAINT `identities_ibfk_1` FOREIGN KEY (`uuid`) REFERENCES `uidentities` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `identities`
--

LOCK TABLES `identities` WRITE;
/*!40000 ALTER TABLE `identities` DISABLE KEYS */;
/*!40000 ALTER TABLE `identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matching_blacklist`
--

DROP TABLE IF EXISTS `matching_blacklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matching_blacklist` (
  `excluded` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`excluded`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matching_blacklist`
--

LOCK TABLES `matching_blacklist` WRITE;
/*!40000 ALTER TABLE `matching_blacklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `matching_blacklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizations`
--

LOCK TABLES `organizations` WRITE;
/*!40000 ALTER TABLE `organizations` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `uuid` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `gender` varchar(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `gender_acc` int(11) DEFAULT NULL,
  `is_bot` tinyint(1) DEFAULT NULL,
  `country_code` varchar(2) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `country_code` (`country_code`),
  CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`uuid`) REFERENCES `uidentities` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `profiles_ibfk_2` FOREIGN KEY (`country_code`) REFERENCES `countries` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uidentities`
--

DROP TABLE IF EXISTS `uidentities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uidentities` (
  `uuid` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_modified` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uidentities`
--

LOCK TABLES `uidentities` WRITE;
/*!40000 ALTER TABLE `uidentities` DISABLE KEYS */;
/*!40000 ALTER TABLE `uidentities` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-20  5:17:42
